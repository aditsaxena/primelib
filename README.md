# PrimeLib

This is my code for a ruby challenge I've been tested for recently.

# Requirements

Write a program that calculates and prints out a multiplication table of the first 10 calculated prime numbers.

Detail: The program must run from the command line and print to screen 1 table.

Across the top and down the left side should be the 10 primes, and the body of the table should contain the product of multiplying these numbers.
 
Please include tests.
 
Think about cases where the code doesn't know the upper limit (maybe it's 30 and not 10 primes?)
Think about code complexity.
Don't use the Ruby prime method.

# The library

## Installation

Gemfile:

    gem 'prime_lib', '~> 0.1.0'

## Usage (in terminal)

    > puts PrimeLib::GeneratorProduct.new(10).to_table


## What I've done

1. Optimization of prime generation based on Sieve of Eratosthenes algorithm
    
    Currently my implementation runs slightly faster[1] than sieve generator of Ruby 2.0 Prime lib[2] which has been greatly improved 6 month ago by Yugui[3].
    
    [1] Ruby Lib takes roughly 1 sec to calculate 200,000 primes, my lib takes 0.6 secs  
    [2] https://github.com/ruby/ruby/blob/trunk/lib/prime.rb  
    [3] https://github.com/ruby/ruby/commit/def83bff91d59fa5fdd1898f903f9000ad1dbe13#diff-cb376f8d9ff85c3dbf97cd71a7463898

2. To maximize performance gain I decided to use C code
    
    > PrimeLib::Generator.new(n_samples, engine: PrimeLib::Generator::SieveOfEratosthenesC.new).to_a
    
