module RegexPrime
  def self.is_prime?(n)
    ('1' * n) !~ /^1?$|^(11+?)\1+$/
  end
end