require 'spec_helper'

describe PrimeLib::Generator do
  
  it "provides a multiplication list of the first N primes" do
    qty = 30
    qty.should be > 1
    
    primes_prods = PrimeLib::GeneratorProduct.new(qty).to_a
    manual_prod = 1
    
    (qty - 1).times do |i|
      manual_prod = manual_prod * primes_prods[i][1]
      expect(manual_prod).to eq primes_prods[i][2]
    end
    
  end
  
end