require 'spec_helper'

describe PrimeLib do
  
  it "Prints a table of first N primes" do
    primelib = PrimeLib::Generator.new(10)
    puts primelib.to_table
  end
  
  it "Prints a table of first N primes products" do
    primelib = PrimeLib::GeneratorProduct.new(10)
    puts primelib.to_table
  end
  
  it "Prints a table of first N primes products" do
    primelib = PrimeLib::GeneratorProduct.new(10, engine: PrimeLib::Generator::SieveOfEratosthenesC.new)
    puts primelib.to_table
  end
  
    # primes = PrimeLib::Generator.new(n_samples, engine: PrimeLib::Generator::SieveOfEratosthenesC.new).to_a
  
end