require 'spec_helper'

describe PrimeLib do

  let(:prime_200_000th) { 2750159 } # http://is.2750159.aprimenumber.com/

  it "compares Ruby 2.0 Prime class" do
    std_class_prime = Prime::EratosthenesGenerator.new
    std_class_last = 200_000.times.map { std_class_prime.next }.last
    expect(std_class_last).to eq prime_200_000th
  end

  it "produces same values as standard Ruby 2.0 Prime class" do
    primelib = PrimeLib::Generator.new(200_000)
    primelib_last = primelib.to_a.last
    expect(primelib_last).to eq prime_200_000th
  end

  it "can speed its calculation time piping directly C code" do
    primelib = PrimeLib::Generator.new(200_000, engine: PrimeLib::Generator::SieveOfEratosthenesC.new)

    primelib_last = primelib.to_a.last
    expect(primelib_last).to eq prime_200_000th
  end

  it "have an performance improvement over its competitor" do
    report = Benchmark.bm(10_000) do |bm|
      bm.report("Prime Ruby 2.0")     { prime  = Prime::EratosthenesGenerator.new ; primes = 1_000_000.times.map { prime.next } ; expect(primes.last).to eq(15485863) }
      bm.report("My implem. in ruby") { primes = PrimeLib::Generator::SieveOfEratosthenes.new(1_000_000).retrieve_primes        ; expect(primes.last).to eq(15485863) }
      bm.report("My implem. inline C"){ primes = PrimeLib::Generator::SieveOfEratosthenesC.new(1_000_000).retrieve_primes       ; expect(primes.last).to eq(15485863) }
    end

    expect(report.min_by { |r| r.real }.label).to eq "My implem. inline C"
  end

end