$LOAD_PATH.unshift(File.dirname(__FILE__))
$LOAD_PATH.unshift(File.join(File.dirname(__FILE__), '..', 'lib'))

require "benchmark"
require "awesome_print"
require "prime_lib"

require "pry"

Dir["#{PrimeLib::ROOT_PATH}/spec/support/**/*.rb"].each { |f| require f }

RSpec.configure do |config|
  config.order = "random" # --seed 1234

  config.filter_run :focus => true
  config.run_all_when_everything_filtered = true

  # Coloring stuff
  config.color_enabled = true # Use color in STDOUT
  config.tty = true

  # config.include MyTestingHelpers #, :type => :controller # http://stackoverflow.com/questions/20614213/how-to-include-a-support-file-in-rails-4-for-rspec

end
