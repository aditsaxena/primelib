require 'inline'
require 'forwardable'
require 'terminal-table'
require 'prime_lib/version'

require 'prime_lib/displayable/table'
require 'prime_lib/displayable/table_product'

require 'prime_lib/generator'
require 'prime_lib/generator_product'
require 'prime_lib/generator/gen_engine'
require 'prime_lib/generator/sieve_of_eratosthenes'
require 'prime_lib/generator/sieve_of_eratosthenes_c'

module PrimeLib
  
  ROOT_PATH = File.expand_path '../..', __FILE__
  
end
