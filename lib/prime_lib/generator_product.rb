module PrimeLib
  
  class GeneratorProduct < Generator
    include PrimeLib::Displayable::TableProduct
    
    def initialize(*args)
      super(*args)
      @product = 1
    end
    
    def each_with_index
      retrieve_primes.each_with_index do |prime, iprime|
        @product *= prime
        yield(iprime, prime, @product)
      end
    end
    
    def to_a
      ret_primes = []
      each_with_index do |iprime, prime, product|
        ret_primes << [iprime, prime, product]
      end
      ret_primes
    end
    
  end
  
end