module PrimeLib
  
  class Generator
    
    class SieveOfEratosthenes < GenEngine
      
      def initialize(max_els = 10)
        super
        @max_checked = @primes.last + 1
      end
      
      def retrieve_primes
        next_batch while primes.size <= max_els
        primes.take(max_els)
      end
      
      private
        
        def next_batch
          max_segment_size = 1e6.to_i
          max_cached_prime = @primes.last

          segment_min = @max_checked
          segment_max = [segment_min + max_segment_size, max_cached_prime * 2].min
          
          root = Math.sqrt(segment_max).floor
          
          sieving_primes = @primes[1..-1].take_while { |prime| prime <= root } # 3 5 7 11 13
          segment = ((segment_min + 1) .. segment_max).step(2).to_a
          
          sieving_primes.each do |sieving_prime| # 102 103 104 105 106 ~ 3 5 7 11 13 -> 1 1 1 9 7
            cursor = (-(segment_min + 1 + sieving_prime) / 2) % sieving_prime
            while cursor <= segment.size
              segment[cursor] = nil
              cursor += sieving_prime
            end
          end
          
          @primes = @primes + segment.compact
          @max_checked = segment_max
        end
        
    end
    
  end
  
end