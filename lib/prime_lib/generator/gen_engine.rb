module PrimeLib
  
  class Generator
    
    class GenEngine
      
      include Enumerable
      
      attr_accessor :max_els
      attr_reader :primes
      
      def initialize(max_els = 10)
        @max_els = max_els
        @primes = [2, 3, 5, 7, 11, 13, 17, 19, 23, 29, 31, 37, 41, 43, 47, 53, 59, 61, 67, 71, 73, 79, 83, 89, 97, 101]
      end
      
      def retrieve_primes
        raise 'define me'
      end
      
    end
    
  end
  
end
