module PrimeLib

  class Generator

    class SieveOfEratosthenesC < GenEngine

      def initialize(max_els = 10)
        super
        @max_checked = @primes.last + 1
      end

      def retrieve_primes
        max_segment_size = 1e6.to_i

        while primes.size <= max_els do
          max_cached_prime = @primes.last

          segment_min = @max_checked
          segment_max = [segment_min + max_segment_size, max_cached_prime * 2].min

          @primes = @primes + next_batch(@primes, segment_min, segment_max).reject{|el| el == false }
          @max_checked = segment_max
        end

        primes.take(max_els)
      end

      inline(:C) do |builder|
        builder.c <<-EOC
          static VALUE next_batch(VALUE primes, double segment_min, double segment_max)
          {
            int i;
            int root = sqrt(segment_max);

            VALUE sieving_primes = rb_ary_new();
            int prime;

            VALUE segment = rb_ary_new();

            int neg_offset, cursor;

            for (i = 1; i < RARRAY_LEN(primes) && (prime = NUM2DBL(rb_ary_entry(primes, i))) <= root; i++) {
              rb_ary_push(sieving_primes, INT2NUM(prime) );
            }

            for (i = segment_min + 1; i < segment_max; i += 2) {
              rb_ary_push(segment, INT2NUM(i));
            }

            for (i = 0; i < RARRAY_LEN(sieving_primes); i++) {
              int sieving_prime = NUM2DBL(rb_ary_entry(sieving_primes, i));

              neg_offset = (-(segment_min + 1 + sieving_prime) / 2);
              cursor = neg_offset + (int)((neg_offset < 0 ? -neg_offset : neg_offset) / sieving_prime) * sieving_prime;
              if (cursor < 0) {
                cursor += sieving_prime;
              }

              while(cursor <= RARRAY_LEN(segment)){
                rb_ary_store(segment, cursor, 0);
                cursor += sieving_prime;
              }
            }

            return segment;
          }
        EOC
      end

    end

  end

end