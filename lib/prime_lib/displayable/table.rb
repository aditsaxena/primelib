module PrimeLib
  
  module Displayable
    
    module Table
      
      def lister_keys
        %W(i Prime)
      end
      
      def lister
        primes = []
        each_with_index do |i, prime|
          primes << [i, prime.to_i]
        end
        primes
      end
      
      def to_table
        ::Terminal::Table.new :headings => lister_keys, :rows => lister
      end
      
    end
    
  end
  
end