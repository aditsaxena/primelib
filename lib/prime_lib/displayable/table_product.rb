module PrimeLib
  
  module Displayable
    
    module TableProduct
      
      def lister_keys
        %W(i Prime Product)
      end
      
      def lister
        primes = []
        each_with_index do |i, prime, product|
          primes << [i, prime, product]
        end
        primes
      end
      
      def to_table
        ::Terminal::Table.new :headings => lister_keys, :rows => lister
      end
      
    end
    
  end
  
end