module PrimeLib
  
  class Generator
    extend Forwardable
    
    include PrimeLib::Displayable::Table
    
    def_delegators :@engine, :retrieve_primes
    
    def initialize(max_els = 100, engine: SieveOfEratosthenes.new)
      @engine = engine
      engine.max_els = max_els
      @max_els = max_els
    end
    
    def method_added(method)
      (class<< self;self;end).def_delegator :instance, method
    end
    
    def each_with_index
      retrieve_primes.each_with_index do |prime, iprime|
        yield(prime, iprime)
      end
    end
    
    def to_a
      retrieve_primes
    end
    
  end
  
end